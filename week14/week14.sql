show variables like 'secure_file_priv';

load data infile '/home/student/Desktop/FKdb/dblab2019/week14/movies.csv'
into table stars fields terminated by ',' enclosed by '"';

load data infile '/home/student/Desktop/FKdb/dblab2019/week14/countries.csv'
into table countries fields terminated by ',';

load data infile '/home/student/Desktop/FKdb/dblab2019/week14/stars.csv'
into table stars fields terminated by ',' enclosed by '"';

load data infile '/home/student/Desktop/FKdb/dblab2019/week14/movie_stars.csv'
into table movie_stars fields terminated by ',';

load data infile '/home/student/Desktop/FKdb/dblab2019/week14/directors.csv'
into table directors fields terminated by ',';

load data infile '/home/student/Desktop/FKdb/dblab2019/week14/movie_directors.csv'
into table movie_directors fields terminated by ',';

load data infile '/home/student/Desktop/FKdb/dblab2019/week14/producer_countries.csv'
into table producer_countries fields terminated by ',';

load data infile '/home/student/Desktop/FKdb/dblab2019/week14/genres.csv'
into table genres fields terminated by ',';

load data infile '/home/student/Desktop/FKdb/dblab2019/week14/languages.csv'
into table languages fields terminated by ',';

