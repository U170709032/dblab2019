create view usa_customers as
select CustomerID, CustomerName, ContactName
from Customers
where Country="USA";

select *
from usa_customers;

select * from usa_customers join Orders on usa_customers.CustomerID = Orders.CustomerID;

create or replace view less_then_avg_price as
select ProductID, ProductName, Price
from Products
where price <(select avg(Price) from Products);


select * from usa_customers join Orders on usa_customers.CustomerID = Orders.CustomerID
where orderID in(
	select orderID 
    from OrderDetails join less_then_avg_price on OrderDetails.ProductID = less_then_avg_price.ProductID
);
